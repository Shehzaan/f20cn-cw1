#Computer Network Security - CW1

##Assessed Coursework 1 - Secret-Key Encryption

### Getting Started

This is a BitBucket backup repository containing of all the files that are needed for this coursework.

### Prerequisites

What things you need to run these files:

* OpenSSL
* Linux PC
* shed hex editor


### Installing

To install `shed`, please type:

```
sudo apt-get install shed
```

## Running the tests

Please follow the instructions in each task folder respectively. Under task X, open the `taskX cmmds.txt` and follow along.

### Running OpenSSL

To do a simple encryption using `OpenSSL`

```
% openssl enc ciphertype -e -in plain.txt -out cipher.bin \
-K 00112233445566778889aabbccddeeff \
-iv 0102030405060708
```

Please replace the ciphertype with a specific cipher type, such as -aes-128-cbc, -aes-128-cfb,
-bf-cbc, etc.

## Author

* **Shehzaan Ansari** - [Shehzaan](https://bitbucket.org/Shehzaan)

## Acknowledgments

* Hat tip to anyone who's code was used
* If you find anything wrong, please tweet at [@Shehzaan96](https://twitter.com/shehzaan96)
* Lemme know how it goes :wink: